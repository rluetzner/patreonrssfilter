using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Text;
using System.Xml.Linq;
using System.Threading;
using System.Linq;

namespace PatreonRssFilter
{
    public static class FilterRssFeed
    {
        private static HttpClient _client = new HttpClient();

        [FunctionName("FilterRssFeed")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string sourceUrl = req.Query["sourceUrl"];            
            string toRemoveString = req.Query["toRemove"];

            if (sourceUrl == null || toRemoveString == null)
            {
                return new BadRequestObjectResult("Please pass a sourceUrl and a list of filter words on the query string");
            }

            string source = "";
            try
            {
                var sourceBytes = Convert.FromBase64String(sourceUrl);
                source = Encoding.UTF8.GetString(sourceBytes);
            }
            catch (FormatException fex)
            {
                return new BadRequestObjectResult($"Given source is not correctly base64 encoded: {fex.Message}");
            }
            catch(DecoderFallbackException dfe)
            {
                return new BadRequestObjectResult($"Decoding the source failed. Make sure it is a base64 encoded UTF-8 string. Error was: {dfe.Message}");
            }


            var toRemove = toRemoveString.Split(';', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.ToLowerInvariant())
                .ToArray();
            
            try
            {                
                var rssFeed = await _client.GetStringAsync(source);                
                var xDoc = XDocument.Parse(rssFeed);
                xDoc.Descendants("item")
                    .Where(e => toRemove.Any(rem => (bool)e.Descendants("title").FirstOrDefault()?.Value?.ToLowerInvariant().Contains(rem)))
                    .Remove();
                var sw = new Utf8StringWriter();
                await xDoc.SaveAsync(sw, SaveOptions.None, CancellationToken.None);
                return new ContentResult
                {
                    Content = sw.ToString(),
                    ContentType = "application/xml"
                };
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }            
        }

        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding => Encoding.UTF8;
        }
    }
}
