# Patreon RSS Filter

This contains an Azure Function that filters a given Patreon RSS feed by given keywords and a Web App to allow for easy URL transformations.

## Function API

The API consists of a single endpoint:

```REST
GET /api/FilterRSSFeed/?provider=artist&auth=1a2b3c4d5e6f7a8b9c0d1e2f3a4b5c6d&toRemove=remove,these,words
```

| Parameter | Type | Description |
|-----------|------|-------------|
| provider  | string | The artist's name on Patreon. |
| auth | string | Your personal authentication key to access your private RSS feed. |
| toRemove | string | A comma separated list of words. |

 All titles containing the given words will be removed from the RSS feed. The data is then resent in the same XML format.

## Web App

The Web App uses an existing Patreon URL to retrieve all current feed information.

The show names are parsed with a regular expression and can be used to apply a filter to the RSS feed.

To query the feed the access key and the Patreon artist is needed. These informations are never stored on the server.

The App is currently localized in english and german.