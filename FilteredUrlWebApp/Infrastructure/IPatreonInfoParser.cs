﻿using FilteredUrlWebApp.Models;

namespace FilteredUrlWebApp.Infrastructure
{
    public interface IRssFeedInfoProvider
    {
        RssFeedInfo Parse(string url);
    }
}