﻿using FilteredUrlWebApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FilteredUrlWebApp.Infrastructure
{
    public class ShowFinder : IShowFinder
    {
        private Regex _showRegex = new Regex(@"^(([a-zA-Z'!,]+\s?)+)\d?:?.*$", RegexOptions.Compiled);

        public IEnumerable<Show> FindShows(XDocument doc)
        {
            return doc
                .Descendants("item")
                .Descendants("title")
                .Select(e => e.Value)
                .Select(e => {
                    var regExVal = _showRegex.Match(e).Groups[1].Value;
                    return string.IsNullOrEmpty(regExVal) ? e : regExVal;
                })                
                .Select(e => e.Trim(' ').ToLowerInvariant())                
                .Distinct()
                .OrderBy(e => e)
                .Select(title => new Show { Title = title});
        }
    }
}
