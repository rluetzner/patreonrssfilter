﻿using FilteredUrlWebApp.Models;
using System;
using System.Text;

namespace FilteredUrlWebApp.Infrastructure
{
    public class RssFeedInfoProvider : IRssFeedInfoProvider
    {    
        public RssFeedInfo Parse(string url)
        {
            var base64String = Convert.ToBase64String(Encoding.UTF8.GetBytes(url));
            return new RssFeedInfo
            {
                SourceUrlBase64 = base64String
            };
        }
    }
}
