﻿using FilteredUrlWebApp.Models;
using System.Collections.Generic;

namespace FilteredUrlWebApp.Infrastructure
{
    public interface IUrlBuilder
    {
        TargetUrl BuildUrl(RssFeedInfo info, List<string> toRemove);
    }
}