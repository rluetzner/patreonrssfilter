﻿using FilteredUrlWebApp.Models;
using System.Collections.Generic;
using System.Xml.Linq;

namespace FilteredUrlWebApp.Infrastructure
{
    public interface IShowFinder
    {
        IEnumerable<Show> FindShows(XDocument doc);
    }
}
