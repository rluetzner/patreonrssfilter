﻿using FilteredUrlWebApp.Models;
using System;
using System.Collections.Generic;

namespace FilteredUrlWebApp.Infrastructure
{
    public class UrlBuilder : IUrlBuilder
    {
        private const string BaseUrl = "https://patreonrssfilter.azurewebsites.net/api/FilterRssFeed";

        public TargetUrl BuildUrl(RssFeedInfo info, List<string> toRemove)
        {
            return new TargetUrl
            {
                
                Url = new Uri($"{BaseUrl}/?sourceUrl={info.SourceUrlBase64}&toRemove={string.Join(';', toRemove)}")
            };
        }
    }
}
