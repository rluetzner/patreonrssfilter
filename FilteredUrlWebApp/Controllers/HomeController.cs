﻿using FilteredUrlWebApp.Infrastructure;
using FilteredUrlWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FilteredUrlWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly HttpClient _client = new HttpClient();
        private readonly IShowFinder _showFinder;
        private readonly IUrlBuilder _urlBuilder;
        private readonly IRssFeedInfoProvider _rssFeedInfoProvider;

        public HomeController(IShowFinder showFinder, IUrlBuilder urlBuilder, IRssFeedInfoProvider patreonParser)
        {
            _showFinder = showFinder;
            _urlBuilder = urlBuilder;
            _rssFeedInfoProvider = patreonParser;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> FindContent(string url)
        {
            var rss = await _client.GetStringAsync(url);
            var xDoc = XDocument.Parse(rss);
            var shows = _showFinder.FindShows(xDoc);
            var info = _rssFeedInfoProvider.Parse(url);
            return View(new ShowViewModel {
                FeedInfo = info,
                Shows = shows
            });
        }

        [HttpPost]
        public IActionResult BuildUrl(string base64Url, List<string> selection)
        {
            var feedInfo = new RssFeedInfo { SourceUrlBase64 = base64Url };
            var url = _urlBuilder.BuildUrl(feedInfo, selection);
            return View(url);
        }
    }
}