﻿using System.Linq;
using System.Threading.Tasks;

namespace FilteredUrlWebApp.Models
{
    public class RssFeedInfo
    {
        public string SourceUrlBase64 { get; set; }
    }
}
