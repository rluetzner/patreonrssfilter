﻿using System;

namespace FilteredUrlWebApp.Models
{
    public class TargetUrl
    {        
        public Uri Url { get; set; }
    }
}
