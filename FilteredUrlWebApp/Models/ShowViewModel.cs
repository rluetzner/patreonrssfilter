﻿using System.Collections.Generic;

namespace FilteredUrlWebApp.Models
{
    public class ShowViewModel
    {
        public RssFeedInfo FeedInfo { get; set; }
        public IEnumerable<Show> Shows { get; set; }
    }
}
